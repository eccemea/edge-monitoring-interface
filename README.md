Details are shown in this PPT presentation (which includes a video): [https://www.dropbox.com/s/entpipljk9kbnie/Edge Monitoring.pptx?dl=1](https://www.dropbox.com/s/entpipljk9kbnie/Edge Monitoring.pptx?dl=1)

# How to use

* Clone this repository
* Run `npm start`
* Create a new [https://help.mypurecloud.com/articles/set-custom-client-application-integration/](Custom Client Application) in PureCloud:
  * Properties
    * Application URL: URL to access this interface (e.g. http://localhost:8080 if you are running the interface on your machine)
    * Application Type: widget
    * Application Category: Leave blank
    * Iframe Sandbox Options: Leave the defaults (allow-scripts,allow-same-origin,allow-forms,allow-modals)
    * Group Filtering: Make sure you select a group where users will have access to this client application
  * Advanced
    * Copy the following in the `Advanced Configuration` text area
```
{
  "icon": {
    "vector": "https://visualpharm.com/assets/811/Server-595b40b75ba036ed117d8edd.svg"
  }
}
```
  * Credentials: Nothing to add here
